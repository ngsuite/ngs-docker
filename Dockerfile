# This file is based on previous work, see https://github.com/schruste/docker-ngsolve

FROM ubuntu:24.04

WORKDIR /home/app

ARG NB_USER=jovyan
ARG NB_UID=1001
ARG NGS_VERSION=6.2.2403
ENV USER ${NB_USER}
ENV NB_UID ${NB_UID}
ENV HOME /home/${NB_USER}

RUN useradd --shell /usr/sbin/nologin \
    --uid ${NB_UID} \
    ${NB_USER}

# RUN export DEBIAN_FRONTEND=noninteractive
RUN apt-get update

# cmake is needed to install e.g. xfem (i'm not sure about that)
RUN apt-get install -y python3-pip cmake git
RUN apt-get install -y python3.12-venv
RUN apt-get update

# it seems like ngsolve-6.2.2303 is not setting LD_LIBRARY_PATH (libiomp5.so not found)
ENV LD_LIBRARY_PATH "$LD_LIBRARY_PATH:/usr/local/lib"

ENV INSTALL_DIR /home/app
WORKDIR ${INSTALL_DIR}
RUN /bin/bash -c "python3 -m venv --clear .venv &&\
                    source ${INSTALL_DIR}/.venv/bin/activate &&\
                    pip3 install --no-cache-dir notebook --no-cache-dir jupyterlab --no-cache-dir ipywidgets --no-cache-dir webgui_jupyter_widgets --no-cache-dir psutil --no-cache-dir numpy --no-cache-dir scipy --no-cache-dir matplotlib --no-cache-dir ipywidgets --no-cache-dir pytest &&\
                    pip3 install --no-cache-dir pybind11_stubgen &&\
                    pip3 install --no-cache-dir ngsolve==${NGS_VERSION}"


ENV PYTHONPATH "${PYTHONPATH}:${INSTALL_DIR}/.venv/lib/python3.12/site-packages"
ENV PATH "${PATH}:${INSTALL_DIR}/.venv/bin"
RUN python3 -c "import ngsolve"

WORKDIR ${HOME}
RUN chown ${NB_UID} ${HOME}

# test if tests run (except cache and solvers using umfpack)
RUN git clone https://github.com/NGSolve/ngsolve.git && cd ngsolve && git checkout v${NGS_VERSION} && cd tests/pytest && pytest -k 'not cache and not newton_with_dirichlet' test_*.py && cd ../../.. && rm -rf ngsolve

# set alias "pip3" to be able to pip-install packages into the virtual environment
RUN echo "alias pip3=\"source ${INSTALL_DIR}/.venv/bin/activate && pip3\"" >> ~/.bashrc

ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /usr/bin/tini
RUN chmod +x /usr/bin/tini
ENTRYPOINT ["/usr/bin/tini", "--"]

USER ${NB_USER}
WORKDIR /home/${NB_USER}

CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root" ]